﻿/*
 *
 * (c) Copyright Ascensio System Limited 2010-2018
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
*/

let path = require("path");
let urlModule = require("url");
let urllib = require("urllib");
let jwt = require("jsonwebtoken");
let jwa = require("jwa");
let fileUtility = require("./fileUtility");
let guidManager = require("./guidManager");
let configServer = require('config').get('server');
let siteUrl = configServer.get('siteUrl');
let cfgSignatureEnable = configServer.get('token.enable');
let cfgSignatureUseForRequest = configServer.get('token.useforrequest');
let cfgSignatureAuthorizationHeader = configServer.get('token.authorizationHeader');
let cfgSignatureAuthorizationHeaderPrefix = configServer.get('token.authorizationHeaderPrefix');
let cfgSignatureSecretExpiresIn = configServer.get('token.expiresIn');
let cfgSignatureSecret = configServer.get('token.secret');
let cfgSignatureSecretAlgorithmRequest = configServer.get('token.algorithmRequest');

let documentService = {};

documentService.userIp = null;

documentService.getConvertedUriSync = function (documentUri, fromExtension, toExtension, documentRevisionId, callback) {
  documentRevisionId = documentService.generateRevisionId(documentRevisionId || documentUri);

  documentService.getConvertedUri(documentUri, fromExtension, toExtension, documentRevisionId, false, function (err, data) {
    if (err) {
      callback();
      return;
    }
    let res = documentService.getResponseUri(data);
    callback(res.value);
  });
};

documentService.getConvertedUri = function (documentUri, fromExtension, toExtension, documentRevisionId, async, callback) {
  fromExtension = fromExtension || fileUtility.getFileExtension(documentUri);

  let title = fileUtility.getFileName(documentUri) || guidManager.newGuid();

  documentRevisionId = documentService.generateRevisionId(documentRevisionId || documentUri);

  let params = {
    async: async,
    url: documentUri,
    outputtype: toExtension.replace(".", ""),
    filetype: fromExtension.replace(".", ""),
    title: title,
    key: documentRevisionId
  };

  let uri = siteUrl + configServer.get('converterUrl');
  let headers = {
    'Content-Type': 'application/json',
    "Accept": "application/json"
  };

  if (cfgSignatureEnable && cfgSignatureUseForRequest) {
    headers[cfgSignatureAuthorizationHeader] = cfgSignatureAuthorizationHeaderPrefix + this.fillJwtByUrl(uri, params);
  }

  urllib.request(uri,
    {
      method: "POST",
      headers: headers,
      data: params
    },
    callback);
};

documentService.generateRevisionId = function (expectedKey) {
  console.log(expectedKey);
  if (expectedKey.length > 20) {
    expectedKey = expectedKey.hashCode().toString();
  }

  let key = expectedKey.replace(new RegExp("[^0-9-.a-zA-Z_=]", "g"), "_");

  return key.substring(0, Math.min(key.length, 20));
};

documentService.processConvertServiceResponceError = function (errorCode) {
  let errorMessage = "";
  let errorMessageTemplate = "Error occurred in the ConvertService: ";

  switch (errorCode) {
    case -20:
      errorMessage = errorMessageTemplate + "Error encrypt signature";
      break;
    case -8:
      errorMessage = errorMessageTemplate + "Error document signature";
      break;
    case -7:
      errorMessage = errorMessageTemplate + "Error document request";
      break;
    case -6:
      errorMessage = errorMessageTemplate + "Error database";
      break;
    case -5:
      errorMessage = errorMessageTemplate + "Error unexpected guid";
      break;
    case -4:
      errorMessage = errorMessageTemplate + "Error download error";
      break;
    case -3:
      errorMessage = errorMessageTemplate + "Error convertation error";
      break;
    case -2:
      errorMessage = errorMessageTemplate + "Error convertation timeout";
      break;
    case -1:
      errorMessage = errorMessageTemplate + "Error convertation unknown";
      break;
    case 0:
      break;
    default:
      errorMessage = "ErrorCode = " + errorCode;
      break;
  }

  throw { message: errorMessage };
};

documentService.getResponseUri = function (json) {
  let fileResult = JSON.parse(json);

  if (fileResult.error)
    documentService.processConvertServiceResponceError(parseInt(fileResult.error));

  let isEndConvert = fileResult.endConvert;

  let percent = parseInt(fileResult.percent);
  let uri = null;

  if (isEndConvert) {
    if (!fileResult.fileUrl)
      throw { message: "FileUrl is null" };

    uri = fileResult.fileUrl;
    percent = 100;
  } else {
    percent = percent >= 100 ? 99 : percent;
  }

  return {
    key: percent,
    value: uri
  };
};

documentService.commandRequest = function (method, documentRevisionId, callback) {
  documentRevisionId = documentService.generateRevisionId(documentRevisionId);
  let params = {
    c: method,
    key: documentRevisionId
  };

  let uri = siteUrl + configServer.get('commandUrl');
  let headers = {
    'Content-Type': 'application/json'
  };
  if (cfgSignatureEnable && cfgSignatureUseForRequest) {
    headers[cfgSignatureAuthorizationHeader] = cfgSignatureAuthorizationHeaderPrefix + this.fillJwtByUrl(uri, params);
  }

  urllib.request(uri,
    {
      method: "POST",
      headers: headers,
      data: params
    },
    callback);
};

documentService.checkJwtHeader = function (req) {
  let decoded = null;
  let authorization = req.get(cfgSignatureAuthorizationHeader);
  if (authorization && authorization.startsWith(cfgSignatureAuthorizationHeaderPrefix)) {
    let token = authorization.substring(cfgSignatureAuthorizationHeaderPrefix.length);
    try {
      decoded = jwt.verify(token, cfgSignatureSecret);
    } catch (err) {
      console.log('checkJwtHeader error: name = ' + err.name + ' message = ' + err.message + ' token = ' + token)
    }
  }
  return decoded;
};

documentService.fillJwtByUrl = function (uri, opt_dataObject, opt_iss, opt_payloadhash) {
  let parseObject = urlModule.parse(uri, true);
  let payload = {query: parseObject.query, payload: opt_dataObject, payloadhash: opt_payloadhash};

  let options = {algorithm: cfgSignatureSecretAlgorithmRequest, expiresIn: cfgSignatureSecretExpiresIn, issuer: opt_iss};
  return jwt.sign(payload, cfgSignatureSecret, options);
};

module.exports = documentService;
