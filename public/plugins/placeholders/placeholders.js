// Example insert text into editors (different implementations)
(function(window, undefined){

  var context;


    window.Asc.plugin.init = function() {
      context = this;

      $('#personTitle').click(function(e) {
        insertText('${person_title}');
      });
      $('#personSalutation').click(function(e) {
        insertText('${person_salutation}');
      });
      $('#personFirstName').click(function(e) {
        insertText('${person_first_name}');
      });
      $('#personName').click(function(e) {
        insertText('${person_name}');
      });
      $('#personAddress').click(function(e) {
        insertText('${person_address}');
      });
      $('#personEmail').click(function(e) {
        insertText('${person_email}');
      });


      $('#employerName').click(function(e) {
        insertText('${employer_name}');
      });
      $('#employerAddress').click(function(e) {
        insertText('${employer_address}');
      });


      $('#centralName').click(function(e) {
        insertText('${central_name}');
      });

      updateScroll();
    };

    window.Asc.plugin.button = function(id)
    {
      this.executeCommand("close", "");
    };

  window.onresize = function() {
    updateScroll();
    updateScroll();
  };


    function insertText(text) {
      window.Asc.plugin.info.recalculate = true;
      window.Asc.plugin.executeMethod("PasteHtml", [text]);
    }

  function updateScroll()
  {
    var container = document.getElementById('scrolling-container');
    Ps.update(container);
    var scrollY = $('.ps__scrollbar-y');
    if($('.ps__scrollbar-y').height() === 0){
      $('.ps__scrollbar-y').css('border-width', '0px');
      $('.ps__scrollbar-y').css('display', 'none');
    }else{
      $('.ps__scrollbar-y').css('border-width', '1px');
      $('.ps__scrollbar-y').css('display', 'block');
    }
    $('.ps__scrollbar-x').css('border-width', '0px');
  }

})(window, undefined);
