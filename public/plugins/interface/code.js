(function() {

  // Hide the header
  $('#header').hide();

  // Hide print and save buttons
  $('#id-toolbar-full-placeholder-btn-print').parent().parent().hide();
  var toolbarSeparators = $('#id-toolbar-full > .separator');
  toolbarSeparators.first().hide();

  // Hide View options and settings
  var bars = $('#id-toolbar-full-placeholder-btn-hidebars');
  bars.hide();
  $('#id-toolbar-full-placeholder-btn-settings').hide();
  bars.parent().parent().css('padding-right', '0');

  // Hide plugin from menu
  $('#asc-gen1607').parent().hide();

  // Hide about button :)
  $('#left-btn-about').hide();

  // Hide print/save/... buttons in file menu
  $('#fm-btn-save').hide();
  $('#fm-btn-edit').hide();
  $('#fm-btn-save-desktop').hide();
  $('#fm-btn-rename').hide();
  $('#fm-btn-print').hide();
  $('#fm-btn-recent').hide();
  $('#fm-btn-create').hide();
  $('#fm-btn-rights').hide();
  $('#fm-btn-history').hide();
  $('#fm-btn-back').hide();
  $('#file-menu-panel > .panel-menu > .devider').last().hide();


  var viewportController;
  try { viewportController = DE.getController('Viewport'); } catch(e) {
    try { viewportController = PE.getController('Viewport'); } catch(e) {
      try { viewportController =  SSE.getController('Viewport'); } catch(e) {}
    }
  }
  if (viewportController) {
    viewportController.onLayoutChanged('toolbar');
    viewportController.onLayoutChanged();
  }

  var afterDocReady = function(){
    // $('#toolbar #id-toolbar-btn-hidebars > ul > li:nth-child(2)').hide();
    // $('#toolbar #id-toolbar-btn-showmode > ul > li:nth-child(2)').hide();
  };

  Common.NotificationCenter.on('document:ready', afterDocReady);
})();
