# onlyoffice-nodejs-middleware

NodeJS middleware for OnlyOffice Document Server. Handles file storage & retrieval. Clients request a document, the middleware retrieves the stored document and sends it to the Document Server for rendering; this returns an HTML page that the middleware forwards to the client.


# Usage

Getting up and running is fairly straightforward:

## Install dependencies

Run `npm install` in the root directory to install all dependencies.

## Set configuration

The default configuration can be found in `config/default.json`. Make sure all values set here are correct.
See [the documentation on Confluence](https://peopleware.atlassian.net/wiki/spaces/CMB/pages/731218141/OnlyOffice+Demo+setup+-+Getting+started) for details.

## Run

Stat the server by running `node bin/www`. This should report an Express server running on port 3000 (or the port of your choice if you changed the port configuration).


